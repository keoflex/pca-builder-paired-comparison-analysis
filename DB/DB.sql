-- phpMyAdmin SQL Dump
-- version 4.0.10.7
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Jun 03, 2016 at 03:14 PM
-- Server version: 5.6.29
-- PHP Version: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `disdt0_pca`
--

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE IF NOT EXISTS `projects` (
  `PROJ_id` bigint(240) NOT NULL AUTO_INCREMENT,
  `PROJ_name` varchar(100) NOT NULL,
  `PROJ_date` date NOT NULL,
  `USR_id` bigint(240) NOT NULL,
  `PROJ_status` int(11) NOT NULL DEFAULT '0' COMMENT '1 is active',
  `PROJ_public` int(1) NOT NULL COMMENT '1 = public',
  `PROJ_time` int(10) NOT NULL DEFAULT '0',
  `PROJ_weight` int(1) NOT NULL DEFAULT '0' COMMENT '0=1, 1=3 2 1',
  `PROJ_emails` longtext NOT NULL,
  `PROJ_button` varchar(100) NOT NULL COMMENT 'Title for buttons',
  PRIMARY KEY (`PROJ_id`),
  KEY `USR_id` (`USR_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30 ;

-- --------------------------------------------------------

--
-- Table structure for table `projects_items`
--

CREATE TABLE IF NOT EXISTS `projects_items` (
  `PITEM_id` bigint(240) NOT NULL AUTO_INCREMENT,
  `PITEM_name` varchar(100) NOT NULL,
  `PITEM_implications` longtext NOT NULL,
  `PITEM_budget` varchar(10) NOT NULL,
  `PROJ_id` bigint(240) NOT NULL,
  PRIMARY KEY (`PITEM_id`),
  KEY `PROJ_id` (`PROJ_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=221 ;

-- --------------------------------------------------------

--
-- Table structure for table `projects_results`
--

CREATE TABLE IF NOT EXISTS `projects_results` (
  `PRES_id` bigint(240) NOT NULL AUTO_INCREMENT,
  `PROJ_id` bigint(240) NOT NULL,
  `PITEM_id` bigint(240) NOT NULL COMMENT 'Current Location',
  `PRES_next` bigint(240) NOT NULL COMMENT 'Next PITEM',
  `PRES_selected` bigint(240) NOT NULL COMMENT 'PITEM_id value for selected',
  `PRES_points` int(1) NOT NULL,
  `USR_id` bigint(250) NOT NULL,
  `PRES_status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`PRES_id`),
  KEY `PROJ_id` (`PROJ_id`,`PITEM_id`,`PRES_selected`),
  KEY `USR_id` (`USR_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8907 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `USR_id` bigint(240) NOT NULL AUTO_INCREMENT,
  `USR_username` varchar(100) NOT NULL,
  `USR_fname` varchar(100) NOT NULL,
  `USR_lname` varchar(100) NOT NULL,
  `USR_pass` varchar(100) NOT NULL DEFAULT 'nopass' COMMENT 'default is sha1 disd2016 and loginseed',
  PRIMARY KEY (`USR_id`),
  UNIQUE KEY `USR_username` (`USR_username`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=64 ;

--
-- Insert admin user with password: mysecurepassword
--
INSERT INTO users(USR_username,USR_fname,USR_lname,USR_pass) values('admin','admin','account','e069e0245c24f3a4e5c57a640112970db8b94527');
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
