  <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Dashboard <small> and Quicklinks</small>
                            
                        </h1>
                    <p style="font-size:18px;"> A <b style="color:#2807F9;">P</b>aired <b style="color:#2807F9;">C</b>omparitive <b style="color:#2807F9;">A</b>nalysis (<b style="color:#2807F9;">PCA</b>) is a tool for developing a priority list among a given set of categories.  When you create a PCA, each category is compared against every other category in order to get an overview of what is most important.<br>
<br>
To get started click "CREATE" below, OR "New PCA" under the Design Center on the left<br>
</p><br>

                    </div>
                </div>
                <!-- /.row -->
<!--
GREAT PLACE FOR AN ALERT
-->

                <!-- /.row -->

                <div class="row">
                    <!--
                    <div class="col-lg-3 col-md-6">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-comments fa-4x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div class="huge">Search</div>
                                        <div>Search Public Matricies</div>
                                    </div>
                                </div>
                            </div>
                            <a href="#">
                                <div class="panel-footer">
                                    <span class="pull-left">View Details</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                    -->
                    <div class="col-lg-3 col-md-6">
                         <a href="./index.php?pg=<?php echo pg_encrypt("DESIGN-new",$pg_encrypt_key,"encode") ?>">
                        <div class="panel panel-green">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-edit fa-4x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div class="huge">Create</div>
                                        <div>a new PCA</div>
                                    </div>
                                </div>
                            </div>
                           
                                <div class="panel-footer">
                                    <span class="pull-left">View Details</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                           
                        </div> </a>
                    </div>
                    <div class="col-lg-3 col-md-6">
                                                    <a href="./index.php?pg=<?php echo pg_encrypt("DESIGN-list",$pg_encrypt_key,"encode") ?>">

                        <div class="panel panel-yellow">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-eye fa-4x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div class="huge">View</div>
                                        <div>Existing PCAs</div>
                                    </div>
                                </div>
                            </div>
                                <div class="panel-footer">
                                    <span class="pull-left">View Details</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            
                        </div></a>
                    </div>
                    
                    <div class="col-lg-3 col-md-6">
                         <a href="./index.php?pg=<?php echo pg_encrypt("MY-history",$pg_encrypt_key,"encode") ?>">

                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-fw fa-history fa-4x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div class="huge">History</div>
                                        <div>Current / Previous</div>
                                    </div>
                                </div>
                            </div>
                                <div class="panel-footer">
                                    <span class="pull-left">View Details</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            
                        </div></a>
                    </div>
                    
                </div>
                <!-- /.row -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Instructions <small> and Overview</small>
                            
                        </h1>
                    <p style="font-size:18px;"> This is a guide to help you understand the function and process of building a Paired Comparative Analysis form.
<br>
</p><br>

                    </div>
                </div>
                <?php
				include "dbcon/localInclude.php";
				?>
                </div>
                <!-- /.row -->