<?php

$Form_complete = 0; // form has not been completed yet
$percentComplete = 0;
$itemcount = 0;
//start processing items by project
$PROJ_id = pg_encrypt($_GET['code'],$pg_encrypt_key,"decode");
$PROJ_id = str_replace('@','',$PROJ_id);
$PROJ_details = "select * from projects where PROJ_id = ".$PROJ_id;
$PROJ_details_res = mysqltng_query($PROJ_details);
$PROJ_status = mysqltng_result($PROJ_details_res,0,"PROJ_status");
$PROJ_name = mysqltng_result($PROJ_details_res,0,"PROJ_name");
$PROJ_weight = mysqltng_result($PROJ_details_res,0,"PROJ_weight");
$PROJ_button = mysqltng_result($PROJ_details_res,0,"PROJ_button");

if($PROJ_status == 0){
	?>
    <div class="row">
		<div class="col-sm-12">
		   
			<div class="panel panel-danger">
				<div class="panel-heading">
					<h1 >OH SNAP! <b><?php echo $PROJ_name; ?></b> is not active!</h1>
				</div>
				<div class="panel-body">
						<h2>Looks like this matrix is not live or has been closed.  Contact the creator to re-activate the options for you!</h2>   
				</div>
			</div>
		</div>
     </div>
    <?php	
}else{
	//get the list of items for the project 
	$PROJ_items = "select * from projects_items where PROJ_id = ".$PROJ_id;
	$PROJ_items_qry = mysqltng_query($PROJ_items);
	$PROJ_array= array();
	$item_count = 0;
	//loop through the items and build an array based on the mysql results
	for($i=0;$i<mysqltng_num_rows($PROJ_items_qry);$i++){
		$PROJ_array[] = mysqltng_fetch_assoc($PROJ_items_qry);
		$item_count = $i;
	}
	
	//Delete previous post
	if(isset($_POST['delete_previous'])){
		mysqltng_query("Delete from projects_results where USR_id = $USR_id and PROJ_id = PROJ_id ORDER BY PRES_id desc limit 1");
	}
	//print_r($PROJ_array);
	//////////////////////////////
	///POST any selections

	if(isset($_POST['val'])){

		$PITEM_selected = $conn->real_escape_string($_POST['PITEM_selected']);
		$PITEM_orig =  $conn->real_escape_string($_POST['PITEM_orig']);
		$PRES_next =  $conn->real_escape_string($_POST['PITEM_next']);
		$PRES_points =  $conn->real_escape_string($_POST['val']);
		
		$POST_result = "insert into projects_results(
		USR_id, 
		PROJ_id,
		PITEM_id,
		PRES_next,
		PRES_selected,
		PRES_points
		 ) values(
		 ".$USR_id.", 
		".$PROJ_id.",
		".$PITEM_orig.",
		".$PRES_next.",
		".$PITEM_selected.",
		".$PRES_points ."
		 )";
		 mysqltng_query($POST_result);
		//echo $_POST['val']." -- ".$_POST['PITEM_selected'];
		//echo $POST_result;	
	}
	//////////////////////////////
	
	//check number of entries already made
	$entries = "select *,
    (SELECT COUNT(*) FROM projects_items WHERE PROJ_id=".$PROJ_id.") AS itemcount,
    (SELECT COUNT(*) FROM projects_results WHERE PROJ_id=".$PROJ_id." and USR_id =".$USR_id.") AS resultcount
	from projects_results where USR_id = ".$USR_id." and PROJ_id = ".$PROJ_id." and PRES_status=1 ORDER BY PRES_id desc limit 1";
	//echo $entries;
	$entries_qry = mysqltng_query($entries);
	$PITEM_id_1 = "";
	$PITEM_name_1 = "";
	$PITEM_implications_1 = "";
		
	$PITEM_id_2 = "";
	$PITEM_name_2 = "";
	$PITEM_implications_2 = "";


	if(mysqltng_num_rows($entries_qry)<1){
		$PITEM_id_1 = $PROJ_array[0]['PITEM_id'];
		$PITEM_name_1 = $PROJ_array[0]['PITEM_name'];
		$PITEM_implications_1 =  $PROJ_array[0]['PITEM_implications'];
		$PITEM_budget_1 =  $PROJ_array[0]['PITEM_budget'];

		$PITEM_id_2 = $PROJ_array[1]['PITEM_id'];
		$PITEM_name_2 = $PROJ_array[1]['PITEM_name'];
		$PITEM_implications_2 =  $PROJ_array[1]['PITEM_implications'];
		$PITEM_budget_2 =  $PROJ_array[1]['PITEM_budget'];

	
	}else{
		$PITEM_id = mysqltng_result($entries_qry,0,"PITEM_id");
		$PITEM_id_loc = last_array_position($PROJ_array,$PITEM_id);
		
		$PRES_next = mysqltng_result($entries_qry,0,"PRES_next");
		$PRES_next_loc = last_array_position($PROJ_array,$PRES_next)+1;
		
		
		if($PRES_next_loc-1==$item_count){
			$PITEM_id_loc++;
			$PRES_next_loc = $PITEM_id_loc+1;
		}
		if($PITEM_id_loc == $item_count){
			$Form_complete =1; //form has been completed.  Do not show options
		}else{

		//echo "<br /><br />".$item_count. " The Next Value is ".$PITEM_id_loc;

		$PITEM_id_1 = $PROJ_array[$PITEM_id_loc]['PITEM_id'];
		$PITEM_name_1 = $PROJ_array[$PITEM_id_loc]['PITEM_name'];
		$PITEM_implications_1 =  $PROJ_array[$PITEM_id_loc]['PITEM_implications'];
		$PITEM_budget_1 =  $PROJ_array[$PITEM_id_loc]['PITEM_budget'];

		$PITEM_id_2 = $PROJ_array[$PRES_next_loc]['PITEM_id'];
		$PITEM_name_2 = $PROJ_array[$PRES_next_loc]['PITEM_name'];
		$PITEM_implications_2 =  $PROJ_array[$PRES_next_loc]['PITEM_implications'];
		$PITEM_budget_2 =  $PROJ_array[$PRES_next_loc]['PITEM_budget'];

		$resultcount = mysqltng_result($entries_qry,0,"resultcount");
		$itemcount = mysqltng_result($entries_qry,0,"itemcount");
		$finalcount = (($itemcount-1)*(($itemcount-1)+1))/2;
		//echo $resultcount." - ".$itemcount." - ".$finalcount."<br>";
		
		$percentComplete = round( ($resultcount / $finalcount)*100,0);
		}
	}
	
	if($Form_complete == 1){
		?>
        <div class="row">
		<div class="col-sm-12">
		   
			<div class="panel panel-success">
				<div class="panel-heading">
					<h1 >Your Done! <b><?php echo $PROJ_name; ?></b> Form complete</h1>
				</div>
				<div class="panel-body">
						<h2>You have completed this Paired Comparison Analysis.  You will not be able to complete it again unless the owner opens it back up. </h2>   
				</div>
			</div>
		</div>
     </div>
        <?php
	}else{
	if($itemcount <> 0){
	?>
        <form  method="post" >
			<button style="width:100%; text-align:center;" name="delete_previous" class="btn btn-danger"  value="1">GO BACK (delete previous answer)</button>

</form>
<br>
<?php
	}
?>
	<div class="row">
		<div class="col-sm-6">
		   
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h2 style="min-height:100px;"><?php echo $PITEM_name_1; ?></h2>
				</div>
				 <div class="panel-body">
					<form name="frmTest" action="matrix.php?code=<?php echo $_GET['code']; ?>" method="post" >
					
                    <input type="hidden" name="PITEM_orig" value="<?php echo $PITEM_id_1; ?>">
                    <input type="hidden" name="PITEM_next" value="<?php echo $PITEM_id_2; ?>">

                    <input type="hidden" name="PITEM_selected" value="<?php echo $PITEM_id_1; ?>">
					<?php
					if($PROJ_weight == 1){
					?>
                    <H3><?php echo $PROJ_button; ?></H3>
				<button style="width:32%;" name="val" id="val-1" type="submit" class="btn btn-success" value="3" onclick="validateFormMultiButton('frmTest');">SIGNIFICANTLY</button>
				<button style="width:32%;"  name="val" id="val-2" class="btn btn-info" value="2" onclick="validateFormMultiButton('frmTest');">MODERATELY</button>
				<button style="width:32%;"  name="val" id="val-3" class="btn btn-warning" value="1" onclick="validateFormMultiButton('frmTest');">SLIGHTLY</button>
                <?php
					}else{
						?>
                        <H3><?php echo $PROJ_button; ?></H3>
						<button style="width:100%;" name="val" id="val" class="btn btn-success" onclick="validateForm('frmTest');"  value="1">CLICK HERE</button> 

						<?php
					}
				?>
                        <span style="width:100%; visibility:hidden;" name="val" id="val-span" class="btn btn-danger"  value="1">Please Wait .....</span>
                    </form>
                    
					
                    <H3>Budget Implications</H3>
	
					<b style="color:#ff0000; font-size:20px;">(<?php echo $PITEM_budget_1; ?>)</b>
			   <H3>Rational</H3>
	
					<?php echo $PITEM_implications_1; ?>
				</div>
			</div>
		</div>
	  
	
		<div class="col-sm-6">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h2 style="min-height:100px;" ><?php echo $PITEM_name_2; ?></h2>
				</div>
				<div class="panel-body">
                	<form name="frmTest2" action="" method="post">
                    	<input type="hidden" name="PITEM_orig" value="<?php echo $PITEM_id_1; ?>">
                    	<input type="hidden" name="PITEM_next" value="<?php echo $PITEM_id_2; ?>">
                        
						<input type="hidden" name="PITEM_selected" value="<?php echo $PITEM_id_2; ?>">
					<?php
					if($PROJ_weight == 1){
					?>
                    <H3><?php echo $PROJ_button; ?></H3>
				<button style="width:32%;"  name="val" id="val2-1" class="btn btn-warning" value="1" onclick="validateFormMultiButton('frmTest2');">SLIGHTLY</button>
                <button style="width:32%;"  name="val" id="val2-2" class="btn btn-info" value="2" onclick="validateFormMultiButton('frmTest2');">MODERATELY</button>
                <button style="width:32%;" name="val" id="val2-3" type="submit" class="btn btn-success" value="3" onclick="validateFormMultiButton('frmTest2');">SIGNIFICANTLY</button>
				
				
                <?php
					}else{
						?>
                        <H3><?php echo $PROJ_button; ?></H3>
						<button style="width:100%;" name="val" id="val2" class="btn btn-success" onclick="validateForm('frmTest2');"  value="1">CLICK HERE</button>

						<?php
					}
					
				?>
                      <span style="width:100%; visibility:hidden;" name="val" id="val2-span" class="btn btn-danger"  value="1">Please Wait .....</span>

                    </form>
                    
                    
                    <H3>Budget Implications</H3>
	
					<b style="color:#ff0000; font-size:20px;">(<?php echo $PITEM_budget_2; ?>)</b>
					<H3>Rational</H3>
	
					<?php echo $PITEM_implications_2; ?>
			   
				</div>
			</div>
		</div>
	</div>
<div class="progress">
  <div class="progress-bar" role="progressbar" aria-valuenow="70"
  aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $percentComplete; ?>%">
    <?php echo $percentComplete; ?>% Complete
  </div>
</div>
    <?php
	
}
}







	?>