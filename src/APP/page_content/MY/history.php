<?php
/************************************************************************************************
Show matrices you created in a list
Author: Michael Keough
Date Modified: 12/3/2015
************************************************************************************************/

?>


		<section>
			<h1>History</h1>
            History of PCAs you have either completed or are currently working on.
			<div class="info">
				<p>&nbsp;</p>
			</div>
			<table id="matrixDT" class="display" cellspacing="0" width="100%">
				<?php
				$th_fields = "
				<th>name</th>
				<th>Percent Complete</th>
				<th>Created</th>
				<th>Reports</th>
				";
				?>
                <thead>
					<tr>
						<?php echo $th_fields; ?>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<?php echo $th_fields; ?>
					</tr>
				</tfoot>
				<tbody>
					
                    <?php
						//$matrixList = "SELECT b.PROJ_name, b.PROJ_id, b.PROJ_status, count(a.PRES_id) as 'number' FROM projects_results a, projects b, users c where a.USR_id = c.USR_id and a.PROJ_id = b.PROJ_id and a.USR_id ='".$USR_id."' GROUP BY b.PROJ_name order by b.PROJ_date desc";
						
						
						$matrixList = "SELECT b.PROJ_name, b.PROJ_id, b.PROJ_status,
    (SELECT COUNT(*) FROM projects_items WHERE PROJ_id=b.PROJ_id) AS itemcount,
    (SELECT COUNT(*) FROM projects_results WHERE PROJ_id=b.PROJ_id and USR_id = a.USR_id) AS resultcount

FROM projects_results a, projects b, users c, projects_items d where a.USR_id = c.USR_id and a.PROJ_id = b.PROJ_id and a.USR_id ='".$USR_id."' and d.PROJ_id = b.PROJ_id GROUP BY b.PROJ_name order by b.PROJ_date desc";
						//echo $matrixList;
						$matrixList_res = mysqltng_query($matrixList);
						
						for($i=0;$i<mysqltng_num_rows($matrixList_res);$i++){
							$matrix_array = mysqltng_fetch_assoc($matrixList_res);
							$PROJ_name = stripcslashes( $matrix_array['PROJ_name']);// matrix_array["PROJ_name"];

							$PROJ_id = stripcslashes( $matrix_array['PROJ_id']);
							$PROJ_code = pg_encrypt($PROJ_id."@",$pg_encrypt_key,"encode");

							$PROJ_date = stripcslashes( $matrix_array['PROJ_date']);
							$date = new DateTime($PROJ_date);
							$PROJ_date = $date->format('m-d-Y');
							//$PROJ_date = str_replace('-','/ ',implode("-", array_reverse(explode("-", $PROJ_date))));						

							$PROJ_status = stripcslashes( $matrix_array['PROJ_status']);
							
							$resultcount = $matrix_array['resultcount'];
							$itemcount = $matrix_array['itemcount'];
							$finalcount = (($itemcount-1)*(($itemcount-1)+1))/2;
							//echo $resultcount." - ".$itemcount." - ".$finalcount."<br>";
							
							$percentComplete = round( ($resultcount / $finalcount)*100,0);
							//echo $percentComplete."% Complete<br>";
							?>
                            <tr>
                                <td>
								
                                <h4><?php echo $PROJ_name ; ?></h4>
                                </td>
                               <td>
								
                                <h4 <?php if($percentComplete < 100) echo "style='color:red;'"; ?>><?php echo $percentComplete."%" ; ?>
                                <?php
								if($percentComplete < 100){

									?>
                                    <a target="new" class="btn btn-success" style="width:50%;margin-left:20px;"  href="<?php echo $BASE_URL; ?>/matrix.php?code=<?php echo $PROJ_code; ?>" />Continue</a>
                                    <?php
								}
								?>
                                </h4>
                                </td>
                                <td><h4><?php echo $PROJ_date ; ?></h4></td>
								
                                <td><h4><a class="btn btn-primary" style="width:100%" href="./?pg=<?php echo pg_encrypt("REPORT-view",$pg_encrypt_key,"encode") ?>&MC=<?php echo $PROJ_code; ?>" />View Report</a>
</h4></td>
                     
							</tr>
                            <?php	
						
						}
					?>
				</tbody>
			</table>
