<?php
/************************************************************************************************
Updates the implications field
Author: Michael Keough
Date Modified: 12/5/2015

Field is calld using the popdiv.js ajaxpage function on an onkeyup event 
document is called from the continue.php file and the matrix_new_field.php file
************************************************************************************************/

  header("Cache-Control: no-cache");
    header("Pragma: nocache");



	include_once "../../dbcon/config_sqli.php";
	$PITEM_id = $conn->real_escape_string($_GET['f1']); 
	$PITEM_budget = $conn->real_escape_string($_GET['f2']);
	$PITEM_budget = preg_replace("/[^0-9,.]/", "", $PITEM_budget);
	//$PITEM_budget = round($PITEM_budget,2);
	//$PITEM_budget = "$".number_format($PITEM_budget);
	//$PITEM_budget = money_format('$%i', $PITEM_budget);
	$update_item = "UPDATE projects_items set PITEM_budget = '".$PITEM_budget."' where PITEM_id = ".$PITEM_id;
	
	if(mysqltng_query($update_item)){
		?>
        <h4 style="color:green; text-align:right">Budget Updated! <?php echo date('H:i:s'); ?></h4>
        <?php
	}else{
		?>
        <h4 style="color:green;text-align:right">ERROR UPDATING BUDGET!</h4>
        <?php	}
?>
