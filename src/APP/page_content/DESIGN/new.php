<?php
/************************************************************************************************
New Matrix Designer form
Author: Michael Keough
Date Modified: 12/3/2015
************************************************************************************************/



if(isset($edit_project_information)){
	?>
     <?php
	  echo $h1_tag;
	  if(isset($edit_project_information)){
		?>  
        <form role="form" action="./index.php?pg=<?php echo pg_encrypt("DESIGN-list",$pg_encrypt_key,"encode") ?>" method="post" enctype="multipart/form-data">
      <input type="hidden" id="post_type" name="post_type" value="<?php echo pg_encrypt("qryMATRIX-copy_qry",$pg_encrypt_key,"encode") ?>" />
      
		<input type="hidden" name="copy_info" value="<?php echo pg_encrypt($copy_pca_qry,$pg_encrypt_key,"encode"); ?>">
        <input type="hidden" name="PROJ_name" value="Copy Of: <?php echo $PROJ_name; ?>">
        <input type="hidden" name="PROJ_button" value="Copy Of: <?php echo $PROJ_button; ?>">
        <input type="hidden" name="PROJ_weight" value="Copy Of: <?php echo $PROJ_weight; ?>">

        <button  style="width:100%;" type="submit" class="btn btn-success">MAKE A COPY OF THIS PCA</button><br><P></P>
        </form>
        <?php
	  }
	  ?>
    <form role="form" method="post" enctype="multipart/form-data">
      <input type="hidden" id="post_type" name="post_type" value="<?php echo pg_encrypt("qryMATRIX-updateProj_qry",$pg_encrypt_key,"encode") ?>" />
      <input type="hidden" name="project_code" value="<?php echo $PROJ_CODE; ?>">
    <?php
	$h1_tag = "<h1>Update this PCA</h1>";
	$button_text = "UPDATE";

}else{
	?>
    <form role="form" action="./?pg=<?php echo pg_encrypt("DESIGN-continue",$pg_encrypt_key,"encode") ?>" method="post" enctype="multipart/form-data">
      <input type="hidden" id="post_type" name="post_type" value="<?php echo pg_encrypt("qryMATRIX-new_qry",$pg_encrypt_key,"encode") ?>" />
    <?php
	$h1_tag = "<h1>CREATE A NEW PCA</h1>";
	$button_text = "CREATE";
	$PROJ_name = '';
	$PROJ_button = 'This item is more important!';

	$PROJ_public = 0;
	$PROJ_time = 0;
	$PROJ_weight = 0;
}
?>

      
      
  <div class="row">
    <div class="col-lg-12">
          
      <div class="form-group">
        <label>Name your PCA</label>
        <input name="matrix_name" type="text" placeholder="Type your PCA name here." class="form-control" value='<?php echo $PROJ_name; ?>'>
        <p class="help-block">This is the name you want people to see.</p>
      </div>
      
      <div class="form-group">
        <label>Button Heading</label>
        <input name="button_header" type="text" placeholder="Type header text" class="form-control" value='<?php echo $PROJ_button; ?>'>
        <p class="help-block">This is the text that appears at the top of the buttons on the selection page. (OPTIONAL)</p>
      </div>
      
    <div class="form-group">
        <label>Make this PCA public?</label>
        <select name="make_public" class="form-control">
            <option <?php if($PROJ_public == 1) echo "selected"; ?> value="1">List this PCA on the public search</option>
            <option <?php if($PROJ_public == 0) echo "selected"; ?> value="0">Hide this PCA from the public search</option>
        </select>
        <p class="help-block">Public PCAs are viewable by anyone with an account in the system.  If you don't make it public you will have to add guests using the email option on the next page.</p>

    </div>
    
       <!--   <div class="form-group">
        <label>Time Limit (in seconds)</label>
        <input name="matrix_time" value="<?php echo $PROJ_time; ?>" type="number" placeholder="Type your matrix name here." class="form-control">
        <p class="help-block">For no time limit use 0.</p>
    </div>
    -->
  

       
    <div class="form-group">
        <label>Point Scale</label>
        <select name="matrix_weight" class="form-control">
            <option <?php if($PROJ_weight == 0) echo "selected"; ?> value="0">Each choice is worth 1 point</option>
            <option <?php if($PROJ_weight == 1) echo "selected"; ?> value="1">Weighted importance (3,2,1)</option>
        </select>
        <p class="help-block">Weighted importance gives users several options when selecting an item.  For instance 
        <ul>
       <li> 1. Slightly more important, </li><li>2. Generally more important, </li><li>3. Much more important</li></ul></p>

    </div>
  </div>
            

</div><button type="submit" class="btn btn-primary"><?php echo $button_text; ?> PCA</button>
</form>
