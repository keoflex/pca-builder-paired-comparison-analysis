<?php
/************************************************************************************************
Continue Editing a Matrix you are building
Author: Michael Keough
Date Modified: 12/5/2015
************************************************************************************************/


/* DELETE AN EXISTING MATRIX ITEM
************************************************************************
Consider moving this to the dbquery main site.  
if del_id is posted item is deleted by PITEM_id
************************************************************************
*/
if(isset($_POST['del_id'])){
	$PITEM_id_delete = $conn->real_escape_string($_POST['del_id']);
	$DEL_QUERY = "DELETE FROM projects_items WHERE PITEM_id = ".$PITEM_id_delete;
	if(mysqltng_query($DEL_QUERY)){
		$delete_results = "DELETE from projects_results where PRES_selected = ".$PITEM_id_delete." OR PRES_next = ".$PITEM_id_delete." OR PITEM_id = ".$PITEM_id_delete;

		mysqltng_query($delete_results);
		?>

<div class="alert alert-success">
  <button type="button" class="close" data-dismiss="alert">×</button>
  <strong>SUCCESS:</strong> The item was deleted! </div>
<?php
	}
}

$PROJ_id = 0;
$ID_Error = 1;
if(isset($_POST['matrix_name'])){
	$PROJ_name = $conn->real_escape_string($_POST['matrix_name']);
	$GetID = "SELECT * FROM projects where PROJ_name ='".$PROJ_name."' and USR_id = $USR_id ORDER BY PROJ_name desc limit 1";
	if($GetID_res=mysqltng_query($GetID)){	
	
		//PROJECT ID
		$PROJ_id = mysqltng_result( $GetID_res,0,"PROJ_id" );
		$ID_Error = 0;
	
	}else{
		$PROJ_id = $PROJ_name."-".$USR_id; //show the project name and user id in the error
	}
}else if(isset($_GET['MC'])){
		$PROJ_id = str_replace("@",'',pg_encrypt($_GET['MC'],$pg_encrypt_key,"decode"));
		$check_id = "select * from projects where PROJ_id = $PROJ_id and USR_id = ".$USR_id;
		if($check_id_res = mysqltng_query($check_id)){
		$ID_Error = 0;
		}else{
		$PROJ_id = "<b>ERROR: Invalid MC Key</b>";
		$ID_Error = 1;
		}

}else{
	$ID_Error =2; // 2 indicates that we need to load a form to search
}

if($ID_Error !== 1){
	if($ID_Error == 2){
		echo "Search form here";
	}else{
	$ProjDATA = "SELECT * FROM projects where PROJ_id ='".$PROJ_id."'";
	$PROJDATA_res = mysqltng_query($ProjDATA);
	
	$PROJ_CODE = pg_encrypt($PROJ_id."@",$pg_encrypt_key,"encode");

	$PROJ_USR_id = mysqltng_result( $PROJDATA_res,0,"USR_id" );
	$PROJ_name = mysqltng_result( $PROJDATA_res,0,"PROJ_name" );
	$PROJ_status = mysqltng_result( $PROJDATA_res,0,"PROJ_status" );
	$PROJ_time = mysqltng_result( $PROJDATA_res,0,"PROJ_time" );
	$PROJ_weight = mysqltng_result( $PROJDATA_res,0,"PROJ_weight" );
	$PROJ_public = mysqltng_result( $PROJDATA_res,0,"PROJ_public" );
	$PROJ_date = mysqltng_result( $PROJDATA_res,0,"PROJ_date" );
	$PROJ_emails = mysqltng_result( $PROJDATA_res,0,"PROJ_emails" );
	$PROJ_button = mysqltng_result( $PROJDATA_res,0,"PROJ_button" );
if($PROJ_USR_id == $USR_id){
	echo "<h1>PCA Code for $PROJ_name is <b style='color:green'>$PROJ_CODE</b></h1>";
?>
<a href='<?php echo $BASE_URL; ?>/matrix.php?code=<?php echo $PROJ_CODE; ?>' target="new"><?php echo $BASE_URL; ?>/matrix.php?code=<?php echo $PROJ_CODE; ?></a>
<div style="clear:both; padding-bottom:20px;"></div>

<!-- Button to return to matrix list --> 
<a style="float:left; margin-right:20px;" class="btn btn-primary"  tabindex="-1" href="./index.php?pg=<?php echo pg_encrypt("DESIGN-list",$pg_encrypt_key,"encode") ?>">PCA List</a> 

<!-- Button to go to report --> 
<a class="btn btn-info" style="margin-right:20px;" href="./?pg=<?php echo pg_encrypt("REPORT-view",$pg_encrypt_key,"encode") ?>&MC=<?php echo $PROJ_CODE; ?>" />View Report</a>
<div style="width:100px; float:left;" id="status_button-<?php echo $PROJ_id; ?>">
  <?php
	  if($PROJ_status == 0){
  ?>
  <a onClick="javascript:ajaxpage('page_content/AJAX_PAGE/matrix_update_status.php', 'status_button-<?php echo $PROJ_id; ?>','<?php echo $PROJ_id; ?>','1');" tabindex="-1"  type="submit" class="btn btn-success">Activate</a>
  <?php
  }else{
	?>
  <a onClick="javascript:ajaxpage('page_content/AJAX_PAGE/matrix_update_status.php', 'status_button-<?php echo $PROJ_id; ?>','<?php echo $PROJ_id; ?>','0');"  type="submit" class="btn btn-warning">Disable</a>
  <?php  
  }
	?>
</div>
<div class="row">
  <div class="col-lg-12">
    <h1>Add Categories to this PCA</h1>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tbody>
        <tr style="text-align:center; font-size:large; font-weight:bold;">
          <td style="width:33%;">ITEM</td>
          <td style="width:33%;">RATIONAL</td>
          <td style="width:33%; ">BUDGET Implications</td>

        </tr>
      </tbody>
    </table>
    <?php
	 //get current items from the database
	$GET_ITEMS = "SELECT * FROM projects_items where PROJ_id = $PROJ_id";
	$GET_ITEMS_res = mysqltng_query($GET_ITEMS);
	$count_id = 0;
	$copy_pca_qry = "INSERT INTO projects_items(PITEM_name,PITEM_implications,PITEM_budget,PROJ_id) values"; //create an insert query for the copy button
	for($i=0;$i<mysqltng_num_rows($GET_ITEMS_res);$i++){
		$ITEMS_Array = mysqltng_fetch_assoc($GET_ITEMS_res);
		//echo $ITEMS_Array['PROJ_name']."project id = ".$ITEMS_Array['PITEM_id']."<br />";
		$copy_pca_qry .= ",('".$ITEMS_Array['PITEM_name']."','".$ITEMS_Array['PITEM_implications']."','".$ITEMS_Array['PITEM_budget']."',PROJ_id_val)";
		?>
    <div id="matrix_field_<?php echo $i; ?>" class="form-group input-group"> 
      
      <!-- Text area for Item Name
************************************************************************
Content is saved autotmatically
ajax page info: php, div id, PITEM, THIS VALUE 
************************************************************************
-->
      <input style="width:40%;background:#FBF9CC;" tabindex="<?php echo $i; ?>" onKeyUp="javascript:ajaxpage('page_content/AJAX_PAGE/matrix_update_field.php', 'saved_progress-<?php echo $i; ?>','<?php echo $ITEMS_Array['PITEM_id']; ?>',this.value);" tabindex="<?php echo $i; ?>" placeholder="Field <?php echo $i; ?>" name="<?php echo $ITEMS_Array['PITEM_id']; ?>-field_input_<?php echo $i; ?>" type="text" class="form-control" value="<?php echo $ITEMS_Array['PITEM_name']; ?>">
      
      <!-- Text area for Item implications
************************************************************************
Content is saved autotmatically
ajax page info: php, div id, PITEM, THIS VALUE 
************************************************************************
-->
      
      <input style="width:40%; " tabindex="<?php echo $i; ?>" onKeyUp="javascript:ajaxpage('page_content/AJAX_PAGE/matrix_update_implications.php', 'saved_progress-<?php echo $i; ?>','<?php echo $ITEMS_Array['PITEM_id']; ?>',this.value);" tabindex="<?php echo $i; ?>" placeholder="Implications <?php echo $i; ?>" name="<?php echo $ITEMS_Array['PITEM_id']; ?>-field_implications_<?php echo $i; ?>" type="text" class="form-control" value="<?php echo $ITEMS_Array['PITEM_implications']; ?>">
      
       <!-- Text area for Budget
************************************************************************
Content is saved autotmatically
ajax page info: php, div id, PITEM, THIS VALUE 
************************************************************************
-->
      
      <input style="width:20%; background:#B6E49E" tabindex="<?php echo $i; ?>" onKeyUp="javascript:ajaxpage('page_content/AJAX_PAGE/matrix_update_budget.php', 'saved_progress-<?php echo $i; ?>','<?php echo $ITEMS_Array['PITEM_id']; ?>',this.value);" tabindex="<?php echo $i; ?>" placeholder="$" name="<?php echo $ITEMS_Array['PITEM_id']; ?>-field_budget_<?php echo $i; ?>" type="text" class="form-control" value="<?php echo $ITEMS_Array['PITEM_budget']; ?>">
      
      <!-- Delete Item
************************************************************************
this Div refreshes the page and runs a query at the top that deletes the 
item by the del_id in the hiddeen imput field
************************************************************************
--> 
      <span style="background:#ffffff !important; border:none !important;" class="input-group-addon">
      <form method="post">
        <input type="hidden" name="del_id" value="<?php echo $ITEMS_Array['PITEM_id']; ?>">
        <input tabindex="-1" type="submit" style="margin-top:-5px;"  class="btn btn-danger" value=" X ">
      </form>
      </span> </div>
    <!-- Save Progress Div
************************************************************************
Content from ajaxpage is loaded into this div when on of the input items
are edited on an onKeyUp event
************************************************************************
-->
    <?php
	 echo "<div id='saved_progress-".$i."'><h4 style='color:green'></h4></div>";
	 $count_id = $i;
	}
	$next_field = $count_id + 1;
	 ?>
    <!-- php, div id, Field #, project id -->
    <div id="matrix_field_<?php echo $next_field; ?>"><a tabindex="-1" class="btn btn-success" href="javascript:ajaxpage('page_content/AJAX_PAGE/matrix_new_field.php', 'matrix_field_<?php echo $next_field; ?>',<?php echo $next_field; ?>,<?php echo $PROJ_id; ?>);">+</a> Add a New Category</div>
  </div>
</div>
<hr>
<!-- Email List for Form
************************************************************************
store emails you would like to send to
************************************************************************
-->
<div class="col-lg-4">
  <div class="panel panel-yellow">
    <div class="panel-heading">
      <h3 class="panel-title"><i class="fa fa-long-arrow-right"></i>Send an Email to participants</h3>
    </div>
    <div class="panel-body">
      <form method="post">
        <input type="hidden" id="post_type" name="post_type" value="<?php echo pg_encrypt("qryMATRIX-projEmails_qry",$pg_encrypt_key,"encode") ?>" />
        <input type="hidden" name="project_code" value="<?php echo $PROJ_CODE; ?>">
        Users will need a unique code to use this PCA.  Enter email addresses below <b style="color:red;">seperated by a comma</b> to send them a custom link.  By doing this they will not need an account to complete the form.
        <?php
				$PROJ_emails = pg_encrypt($PROJ_emails,$pg_encrypt_key,"decode");
				$PROJ_emails = str_replace(',\r\n',',,',$PROJ_emails);
				$PROJ_emails = str_replace(',',',&#13;&#10;',$PROJ_emails);
				$PROJ_emails = str_replace(',&#13;&#10;,&#13;&#10;',',&#13;&#10;',$PROJ_emails);
				?>
        <textarea rows="10" class="form-control" name="email_list"><?php echo  $PROJ_emails;
 ?></textarea>
        <br>
        <input tabindex="-1" name="send" type="submit" style="margin-top:-5px;"  class="btn btn-primary" value="Save">
        <input tabindex="-1" name="send" type="submit" style="margin-top:-5px;"  class="btn btn-success" value="Save & Send">
      </form>
      
    </div>
  </div>
</div>
<div class="col-lg-8">
  <div class="panel panel-red">
    <div class="panel-heading">
      <h3 class="panel-title"><i class="fa fa-long-arrow-right"></i> Modify PCA Details</h3>
    </div>
    <div class="panel-body">
      <?php 
		   $edit_project_information = true;
		   include "page_content/DESIGN/new.php"; ?>
    </div>
  </div>
</div>




<div class="col-lg-12">
  <div class="panel panel-yellow">
    <div class="panel-heading">
      <h3 class="panel-title"><i class="fa fa-long-arrow-right"></i>Links for Members</h3>
    </div>
    <div class="panel-body">
      <div class="accordion" id="accordion2">
          <div class="accordion-group">
            <div class="accordion-heading">
              <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse<?php echo $USR_result_id; ?>">
               <h4>Click to show links</h4>
                </b>
              </a>
            </div>
            <div id="collapse<?php echo $USR_result_id; ?>" class="accordion-body collapse">
              <div class="accordion-inner">
                 <?php 
				 $explode_list = explode(",",$PROJ_emails);
				 foreach($explode_list as $email){
					echo "<h4>".$email."</h4>";
					if(strlen($email)>3){
					echo "<span style='width:100%; overflow:auto;'><input class='form-control' type='text' readonly value='".$BASE_URL."/matrix.php?code=".$PROJ_CODE."&email=".pg_encrypt($email,$pg_encrypt_key,"encode")."'></span>";
					}
				 }
				 ?>
              </div>
            </div>
          </div>
        </div>
    </div>
      
  </div>
</div>
<?php
}else{
	echo "<h1>YOU CAN NOT EDIT A PROJECT YOU DID NOT CREATE!</h1>";
	include "page_content/index.php";
}
	}
	
}else{
	//Error Locating ID
	?>
<div class="alert alert-danger">
  <button type="button" class="close" data-dismiss="alert">×</button>
  <strong>ERROR!!: </strong> Something went wrong:  We could not find your PCA.  Please notify the system admin!  The ID for the Matrix we were looking for was (<?php echo $PROJ_id; ?>) </div>
<?php
}
/*
?>

<form role="form">
  <div class="row">
    <div class="col-lg-6">
      <h1>CONTINUE BUILDING MATRIX</h1>
      <div class="form-group">
        <label>Name your Matrix</label>
        <input name="user_first_name" type="text" value="<?php echo $fname; ?>" class="form-control">
        <p class="help-block">This is the name you want people to see.</p>
      </div>
      
    <div class="form-group">
        <label>Make this Matrix public?</label>
        <select class="form-control">
            <option>List this matrix on the public search</option>
            <option>Hide this matrix from the public search</option>
         
        </select>
    </div>
       

    </div>
  </div><button type="submit" class="btn btn-primary">CREATE MATRIX</button>
</form>
*/

//This is not the user that created the project
?>
