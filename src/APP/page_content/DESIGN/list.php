<?php
/************************************************************************************************
Show matrices you created in a list
Author: Michael Keough
Date Modified: 12/3/2015
************************************************************************************************/
?>

		<section>
			<h1>PCAs you have created</h1>
			<div class="info">
				<p>&nbsp;</p>
			</div>
			<table id="matrixDT" class="display" cellspacing="0" width="100%">
				<?php
				$th_fields = "
				<th>name</th>
				<th>Code / Link</th>
				<th>Created</th>
				<th>Status</th>
				<th>Reports</th>
				<th>Delete</th>
				";
				?>
                <thead>
					<tr>
						<?php echo $th_fields; ?>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<?php echo $th_fields; ?>
					</tr>
				</tfoot>
				<tbody>
					
                    <?php
						$matrixList = "SELECT * FROM projects where USR_id ='".$USR_id."' order by PROJ_date desc";
						$matrixList_res = mysqltng_query($matrixList);
						
						for($i=0;$i<mysqltng_num_rows($matrixList_res);$i++){
							$matrix_array = mysqltng_fetch_assoc($matrixList_res);
							$PROJ_name = stripcslashes( $matrix_array['PROJ_name']);// matrix_array["PROJ_name"];

							$PROJ_id = stripcslashes( $matrix_array['PROJ_id']);
							$PROJ_code = pg_encrypt($PROJ_id."@",$pg_encrypt_key,"encode");

							$PROJ_date = stripcslashes( $matrix_array['PROJ_date']);
							$date = new DateTime($PROJ_date);
							$PROJ_date = $date->format('m-d-Y');
							
							//$PROJ_date = str_replace('-','/ ',implode("-", array_reverse(explode("-", $PROJ_date))));						

							$PROJ_status = stripcslashes( $matrix_array['PROJ_status']);
							

							?>
                            <tr>
                                <td>
								
                                <a class="btn btn-success" style="width:100%" href="./?pg=<?php echo pg_encrypt("DESIGN-continue",$pg_encrypt_key,"encode") ?>&MC=<?php echo $PROJ_code; ?>" /><h4><?php echo $PROJ_name ; ?></h4></a>
                                </td>
                                <td>
                                     
     									 <a class="btn btn-primary" style="width:100%" href="<?php echo $BASE_URL; ?>/matrix.php?code=<?php echo $PROJ_code; ?>" target="new"><?php echo $PROJ_code ; ?></a>

								</td>
                                <td><h4><?php echo $PROJ_date ; ?></h4></td>
                                <td>
								<div id="status_button-<?php echo $PROJ_id; ?>">
								<?php 
							
	  if($PROJ_status == 0){
  ?>
  
  <a onClick="javascript:ajaxpage('page_content/AJAX_PAGE/matrix_update_status.php', 'status_button-<?php echo $PROJ_id; ?>','<?php echo $PROJ_id; ?>','1');" tabindex="-1" type="submit" class="btn btn-success">Activate</a>
<?php
  }else{
	?>
  <a onClick="javascript:ajaxpage('page_content/AJAX_PAGE/matrix_update_status.php', 'status_button-<?php echo $PROJ_id; ?>','<?php echo $PROJ_id; ?>','0');" type="submit" class="btn btn-warning">Disable</a>
    <?php  
  }
	?></div>
                                </td>
                                <td><h4><a class="btn btn-primary" style="width:100%" href="./?pg=<?php echo pg_encrypt("REPORT-view",$pg_encrypt_key,"encode") ?>&MC=<?php echo $PROJ_code; ?>" />View Report</a>
</h4></td>
                                <td> 
                                  <form role="form" action="./?pg=<?php echo pg_encrypt("DESIGN-list",$pg_encrypt_key,"encode") ?>" method="post" enctype="multipart/form-data">
      <input type="hidden" id="post_type" name="post_type" value="<?php echo pg_encrypt("qryMATRIX-deleteProj",$pg_encrypt_key,"encode") ?>" />
                               <input type="hidden" name="delid" value="<?php echo $PROJ_id; ?>">          
                                <input type="submit" class="btn btn-danger" style="width:100%" href="#" target="new" Value="DELETE">
                                </form>
</td>
							</tr>
                            <?php	
						
						}
					?>
				</tbody>
			</table>
