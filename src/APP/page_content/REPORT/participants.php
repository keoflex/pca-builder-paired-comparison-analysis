
<div class="row">
  <div class="col-lg-12">
    <h1 class="page-header"> Reports 
    <a class="btn btn-primary"  tabindex="-1" href="./index.php?pg=<?php echo pg_encrypt("REPORT-view",$pg_encrypt_key,"encode") ?>&MC=<?php echo $_GET['MC']; ?>&data=all">ALL</a>
    
    <a class="btn btn-primary"  tabindex="-1" href="./index.php?pg=<?php echo pg_encrypt("REPORT-view",$pg_encrypt_key,"encode") ?>&MC=<?php echo $_GET['MC']; ?>&data=pie">PIE</a>
    
    <a class="btn btn-primary"  tabindex="-1" href="./index.php?pg=<?php echo pg_encrypt("REPORT-view",$pg_encrypt_key,"encode") ?>&MC=<?php echo $_GET['MC']; ?>&data=bar">BAR</a>
    
    <a  class="btn btn-primary" tabindex="-1" href="./index.php?pg=<?php echo pg_encrypt("REPORT-view",$pg_encrypt_key,"encode") ?>&MC=<?php echo $_GET['MC']; ?>&data=table">TABLE</a>
    </h1>
  </div>
</div>
<a href='<?php echo $BASE_URL; ?>/matrix.php?code=<?php echo $_GET['MC']; ?>' target="new"><?php echo $BASE_URL; ?>/matrix.php?code=<?php echo $_GET['MC']; ?></a>
<div style="clear:both; padding-bottom:20px;"></div>

<!-- Button to return to matrix list -->
<a style="float:left; margin-right:20px;" class="btn btn-primary"  tabindex="-1" href="./index.php?pg=<?php echo pg_encrypt("DESIGN-list",$pg_encrypt_key,"encode") ?>">PCA List</a>

<!-- Button to go to report -->
<a class="btn btn-info" style="margin-right:20px;" href="./?pg=<?php echo pg_encrypt("REPORT-view",$pg_encrypt_key,"encode") ?>&MC=<?php echo $_GET['MC']; ?>" />Full Report</a>


<div style="padding-bottom:20px;"></div>
<?php
	if(isset($_GET['MC'])){
		$PROJ_id = str_replace("@",'',pg_encrypt($_GET['MC'],$pg_encrypt_key,"decode"));
?>

		<section>
			<h1>PCA Results by User</h1>
			<div class="info">
				<p>&nbsp;</p>
			</div>
			<table id="matrixDT" class="display" cellspacing="0" width="100%">
				<?php
				$th_fields = "
				<th>User Name</th>
				<th>View Choices</th>
				<th>Delete Choices</th>
				<th>Show Choices</th>

				";
				?>
                <thead>
					<tr>
						<?php echo $th_fields; ?>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<?php echo $th_fields; ?>
					</tr>
				</tfoot>
				<tbody>
					
                    <?php
						/*$userliset = "select a.USR_username,a.USR_id from users a,projects_results b
		where b.PROJ_id = ".$PROJ_id." and b.USR_id = a.USR_id Group by a.USR_username ORDER BY a.USR_username asc";
		*/
		$userliset = "select count(b.PRES_id) as 'num_completed' ,a.USR_username,a.USR_id, c.USR_id as 'PROJ_USR_id',
		(select count(PITEM_id) from projects_items where PROJ_id = ".$PROJ_id.") as 'PROJ_count'
		 from users a,projects_results b, projects c
		where b.PROJ_id = ".$PROJ_id." and b.USR_id = a.USR_id and b.PROJ_id = c.PROJ_id Group by a.USR_username ORDER BY a.USR_username asc";
		//echo $userliset;
						$userliset_res = mysqltng_query($userliset);
						
						for($i=0;$i<mysqltng_num_rows($userliset_res);$i++){
							$matrix_array = mysqltng_fetch_assoc($userliset_res);
							$USR_result_username = stripcslashes( $matrix_array['USR_username']);// matrix_array["PROJ_name"];
							$USR_result_id = stripcslashes( $matrix_array['USR_id']);
							$PROJ_USR_id = stripcslashes( $matrix_array['PROJ_USR_id']);
							$PRES_count = stripcslashes( $matrix_array['num_completed']);
							$PROJ_count = stripcslashes( $matrix_array['PROJ_count']);
							$PROJ_count = (($PROJ_count-1)*($PROJ_count))/2;
							$percent_complete = round(($PRES_count/$PROJ_count)*100,2);
						
							if($percent_complete == 100){
								 $per_color = "green";
							}else{
								 $per_color = "red";
							}
							?><br>

                            <tr>
                                <td><h3><?php echo $USR_result_username.": <b style='color:".$per_color."'>".$percent_complete."% Complete"; echo $PRES_count." ".$PROJ_count; ?></b></h3></td>
                                <td><a class="btn btn-primary" style="width:100%" href="./index.php?pg=<?php echo pg_encrypt("REPORT-view",$pg_encrypt_key,"encode") ?>&U=<?php echo pg_encrypt($USR_result_id."@",$pg_encrypt_key,"encode") ?>&MC=<?php echo $_GET['MC']; ?>">VIEW</a>
</td>
                                <td>
                                <?php
								if($PROJ_USR_id == $USR_id){
								?>
                                <form role="form"  method="post" enctype="multipart/form-data">
      <input type="hidden" id="post_type" name="post_type" value="<?php echo pg_encrypt("qryREPORTS-deleteEntries",$pg_encrypt_key,"encode") ?>" />
                               <input type="hidden" name="proj" value="<?php echo $PROJ_id; ?>"> 
                               <input type="hidden" name="delid" value="<?php echo $USR_result_id; ?>">          
                                <input type="submit" class="btn btn-danger" style="width:100%" href="#" target="new" Value="DELETE">
                                </form>
                                <?php
								}
								?>
                                
</td>
                               <td>
                               <div class="accordion" id="accordion2">
                                  <div class="accordion-group">
                                    <div class="accordion-heading">
                                      <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse<?php echo $USR_result_id; ?>">
                                       <h4>TOP 5: Click Here to <b style="color:#408707">SHOW</b> / <b style="color:#F50D11">HIDE</h4>
                                        </b>
                                      </a>
                                    </div>
                                    <div id="collapse<?php echo $USR_result_id; ?>" class="accordion-body collapse">
                                      <div class="accordion-inner">
                                          <?php include "page_content/REPORT/view_table.php"; ?>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                               </td>

							</tr>
                            <?php	
						
						}
					?>
				</tbody>
			</table>
<?php
	}
?>