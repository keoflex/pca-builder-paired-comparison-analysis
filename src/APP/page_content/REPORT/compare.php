
<div class="row">
  <div class="col-lg-12">
   <?php
	$U = '';
	$U_qry = '';
	if(isset($_GET['U'])){
		$Uval = str_replace("@","",pg_encrypt($_GET['U'],$pg_encrypt_key,"decode"));
		$U = "&U=".$_GET['U'] ;
		$U_qry = " and a.USR_id = ".$Uval ;
		$username_from_res = "select * from users where USR_id = ".$Uval;
		$username_from_res_QRY = mysqltng_query($username_from_res);
		$USR_username_button_val = mysqltng_result($username_from_res_QRY,0,"USR_username");
		?>
		<a class="btn btn-danger"  style="width:100%;" tabindex="-1" href="./index.php?pg=<?php echo pg_encrypt("REPORT-view",$pg_encrypt_key,"encode") ?>&MC=<?php echo $_GET['MC']; ?>">( <?php echo $USR_username_button_val; ?> ) EXIT AND RETURN TO SUMMARY VIEW</a>
        <?php
	}
	?>
    <h1 class="page-header"> Reports 
   
    <a class="btn btn-primary"  tabindex="-1" href="./index.php?pg=<?php echo pg_encrypt("REPORT-view",$pg_encrypt_key,"encode") ?>&MC=<?php echo $_GET['MC']; ?>&data=all<?php echo $U; ?>">ALL</a>
    
    <a class="btn btn-primary"  tabindex="-1" href="./index.php?pg=<?php echo pg_encrypt("REPORT-view",$pg_encrypt_key,"encode") ?>&MC=<?php echo $_GET['MC']; ?>&data=pie<?php echo $U; ?>">PIE</a>
    
    <a class="btn btn-primary"  tabindex="-1" href="./index.php?pg=<?php echo pg_encrypt("REPORT-view",$pg_encrypt_key,"encode") ?>&MC=<?php echo $_GET['MC']; ?>&data=bar<?php echo $U; ?>">BAR</a>
    
    <a  class="btn btn-primary" tabindex="-1" href="./index.php?pg=<?php echo pg_encrypt("REPORT-view",$pg_encrypt_key,"encode") ?>&MC=<?php echo $_GET['MC']; ?>&data=table<?php echo $U; ?>">TABLE</a>
    
    <a  class="btn btn-warning" tabindex="-1" href="./index.php?pg=<?php echo pg_encrypt("REPORT-compare",$pg_encrypt_key,"encode") ?>&MC=<?php echo $_GET['MC']; ?>&data=table<?php echo $U; ?>">COMPARE</a>
    </h1>
  </div>
</div>
<div style="clear:both; padding-bottom:20px;"></div>

<!-- Button to return to matrix list -->
<a style="float:left; margin-right:20px;" class="btn btn-primary"  tabindex="-1" href="./index.php?pg=<?php echo pg_encrypt("DESIGN-list",$pg_encrypt_key,"encode") ?>">PCA List</a>

<!-- Button to go to report -->
<a class="btn btn-success" style="margin-right:20px;" href="./?pg=<?php echo pg_encrypt("DESIGN-continue",$pg_encrypt_key,"encode") ?>&MC=<?php echo $_GET['MC']; ?>" />Edit Fields</a>

<a class="btn btn-info" style="margin-right:20px;" href="./?pg=<?php echo pg_encrypt("REPORT-participants",$pg_encrypt_key,"encode") ?>&MC=<?php echo $_GET['MC']; ?>" />Participants</a>
<div style="padding-bottom:20px;"></div>
<hr>
<form action="" method="get">
<input type="hidden" name="pg" value="<?php echo $_GET['pg']; ?>">
<select name="MC">
<?php
//lOAD PCH LIST
	$matrixList = "SELECT * FROM projects where USR_id ='".$USR_id."' order by PROJ_name asc";
	$matrixList_res = mysqltng_query($matrixList);
	
	for($i=0;$i<mysqltng_num_rows($matrixList_res);$i++){
		$matrix_array = mysqltng_fetch_assoc($matrixList_res);
		$PROJ_name = stripcslashes( $matrix_array['PROJ_name']);// matrix_array["PROJ_name"];

		$PROJ_id = stripcslashes( $matrix_array['PROJ_id']);
		$PROJ_code = pg_encrypt($PROJ_id."@",$pg_encrypt_key,"encode");		
		//$PROJ_date = str_replace('-','/ ',implode("-", array_reverse(explode("-", $PROJ_date))));						
		$PROJ_status = stripcslashes( $matrix_array['PROJ_status']);
		
		if (strpos($_GET['MC'],$PROJ_code) === false) {

		?>
        <option value="<?php echo rtrim( $PROJ_code."-".$_GET['MC'],'-'); ?>"><?php echo $PROJ_name; ?></option>
        <?php
		}
	}
	
?>
</select>
    <input type="submit" class="btn btn-success" value="COMPARE">

</form>
<hr>

<div id="printarea">
 <div class="row">
<?php

if(isset($_GET['MC'])){	
	$MC_Val = $_GET['MC'];
	$MC_exp = explode('-',$MC_Val);
	foreach($MC_exp as $PROJ_id){
		$MC_CODE =  str_replace("@",'',$PROJ_id);
		$PROJ_id = str_replace("@",'',pg_encrypt($PROJ_id,$pg_encrypt_key,"decode"));
		$check_id = "select b.PITEM_budget, b.PITEM_implications, c.PROJ_name, c.PROJ_id, b.PITEM_name, a.PRES_selected, sum(a.PRES_points) as points from projects_results a, projects_items b, projects c where a.PROJ_id = ".$PROJ_id." and a.PRES_selected = b.PITEM_id and a.PROJ_id = c.PROJ_id ".$U_qry." Group by a.PRES_selected ORDER BY points desc";
	
		if($check_id_res = mysqltng_query($check_id)){
			$ID_Error = 0;
		}else{
			$PROJ_id = "<b>ERROR: Invalid MC Key</b>";
			$ID_Error = 1;
		}
	
		if($ID_Error == 0){
			////echo "test";
			$tableData = '';
			$tableData_morris = '';
			$tableData_table = '';
			$PROJ_name = '';
			$percent_color = "1";
			$previous_sum = 0;
			$table_rows = mysqltng_num_rows($check_id_res);
			for($i=0;$i<$table_rows;$i++){
				$itemArray = mysqltng_fetch_assoc($check_id_res);
				////echo "<br />PROJECT=".$itemArray['PROJ_name']." ITEM=".$itemArray['PITEM_name']." POINTS=".$itemArray['points'];	
				
				//store data for the TABLE
				$percent_color = ($table_rows-$i)/$table_rows;
				$text_color = 'yellow';
				if($percent_color <.5){
					$text_color = 'black';	
				}
				$tableData_table .= "
				<tr>
					<th style='background:#97CAEF;'>".$itemArray['PITEM_name']."</th>
					<td style='color:".$text_color.";font-size:18px;background:".colourBrightness('#2D8745',$percent_color)."'>".$itemArray['points']."</td>
					<td>".$itemArray['PITEM_budget']."</td>
				</tr>
				
				";
				if($previous_sum != $itemArray['points']){
					$percent_color -= .10;
				}
				//set the project name
				$PROJ_name = $itemArray['PROJ_name'];
				$PROJ_name = $itemArray['PROJ_name'];
		
				$previous_sum = $itemArray['points'];
			}
		?>
		
		
                    <div class="col-lg-3 text-center">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="table-responsive">
                                <?php
								$trimCode = str_replace($MC_CODE ,'',$_GET['MC']);
								$trimCode = str_replace("--","-",$trimCode);
								$trimCode = rtrim($trimCode,"-");
								?>
                                    <h4><b><?php echo $PROJ_name; ?></b></h4><a style="color:red;" href="index.php?pg=<?php echo $_GET['pg']."&MC=".$trimCode; ?>"><i class="fa fa-fw fa-close"></i> CLOSE</a>
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                            <tr style="background:#373737; color:#FFFFFF;" >
                                                <th>Category</th>
                                                <th>Score</th>
                                                <th>Budget</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            echo $tableData_table;
                                            ?>                                   
                                        </tbody>
                                    </table>
                            	</div>
                            </div>
                        </div>
                    </div>
                    
                <!--
		<div class="row">
			<div  class="col-lg-3">
				<h4><b><?php echo $PROJ_name; ?></b></h4>
				<div class="table-responsive">
					<table class="table table-bordered table-hover">
						<thead>
							<tr style="background:#373737; color:#FFFFFF;" >
								<th>Category</th>
								<th>Score</th>
								<th>Budget</th>
							</tr>
						</thead>
						<tbody>
							<?php
							echo $tableData_table;
							?>                                   
						</tbody>
					</table>
				</div>
			</div>
		-->
		
	<?php
		}
	}
}else{
echo "<h3>No Tables to Load</h3>";	
}
?>
</div><!-- Close row -->
</div><!-- close printarea -->