

    <?php
		$ID_Error = 0;
	if(isset($USR_result_id)){
		$check_id = "select b.PITEM_budget, b.PITEM_implications, c.PROJ_name, c.PROJ_id, b.PITEM_name, a.PRES_selected, sum(a.PRES_points) as points from projects_results a, projects_items b, projects c where a.PROJ_id = ".$PROJ_id." and a.PRES_selected = b.PITEM_id and a.PROJ_id = c.PROJ_id and a.USR_id = ".$USR_result_id." Group by a.PRES_selected ORDER BY points desc limit 5";
		//echo $check_id;
		
		if($check_id_res = mysqltng_query($check_id)){
		$ID_Error = 0;
		}else{
		$PROJ_id = "<b>ERROR: Invalid MC Key</b>";
		$ID_Error = 1;
		}
}
if($ID_Error == 0){
	////echo "test";
	$tableData = '';
	$tableData_morris = '';
	$tableData_table = '';
	$PROJ_name = '';
	$percent_color = "1";
	$previous_sum = 0;
	for($a=0;$a<mysqltng_num_rows($check_id_res);$a++){
		$itemArray = mysqltng_fetch_assoc($check_id_res);
		////echo "<br />PROJECT=".$itemArray['PROJ_name']." ITEM=".$itemArray['PITEM_name']." POINTS=".$itemArray['points'];	
		//store data for the PIE Chart
		$tableData .= " { 
		label: \"<span style='font-size:24px; padding-left:5px;'> ".$itemArray['points']." - </span>  <b style='font-size:18px;'>".$itemArray['PITEM_name']."</b>\",
        data: ".$itemArray['points']."
    },";
	
		
		//store data for the TABLE
		$tableData_table .= "
		<tr>
			<th style='background:#97CAEF;'>".$itemArray['PITEM_name']."</th>
			<td style='color:yellow;font-size:18px;background:".colourBrightness('#2D8745',$percent_color)."'>".$itemArray['points']."</td>
			<td>".$itemArray['PITEM_budget']."</td>

		</tr>
		
		";
			

		if($previous_sum != $itemArray['points']){
			$percent_color -= .10;
		}
		//set the project name
		$PROJ_name = $itemArray['PROJ_name'];
		$PROJ_name = $itemArray['PROJ_name'];

		$previous_sum = $itemArray['points'];
	}

?>
      <div  class="col-lg-12">
        <div class="table-responsive">
          <table class="table table-bordered table-hover">
            <thead>
              <tr style="background:#373737; color:#FFFFFF;" >
                <th>Category</th>
                <th>Score</th>
                <th>Budget</th>
              </tr>
            </thead>
            <tbody>
              <?php
                    echo $tableData_table;
                    ?>
            </tbody>
          </table>
        </div>
      </div>

<?php
}
?>
