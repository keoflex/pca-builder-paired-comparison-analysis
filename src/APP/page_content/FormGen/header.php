  <?php
  include "page_content/header.php";
  ?>
  <script src="js/form/jquery.min.js"></script>
  <script src="js/form/jquery-ui.min.js"></script>
  <script src="js/form/formBuilder.js"></script>
  <link rel="stylesheet" href="css/formBuilder.css">
  <script>
  jQuery(document).ready(function($) {
    'use strict';
    $('textarea').formBuilder();
  });
  </script>