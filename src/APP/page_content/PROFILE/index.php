<?php
include_once("page_content/header.php");
?>

      <form action="./?pg=<?php echo pg_encrypt("PROFILE-index",$pg_encrypt_key,"encode") ?>" method="post" enctype="multipart/form-data">
              <input type="hidden" id="post_type" name="post_type" value="<?php echo pg_encrypt("qryPROFILE-update_qry",$pg_encrypt_key,"encode") ?>" />


  <div class="row">
    <div class="col-lg-6">
      <h1>User Information</h1>
      <div class="form-group">
        <label>First name</label>
        <input name="user_first_name" type="text" value="<?php echo $fname; ?>" class="form-control">
        <p class="help-block">The name yo momma gave ya.</p>
      </div>
      <div class="form-group">
        <label>Last Name</label>
        <input name="user_last_name" type="text" value="<?php echo $lname; ?>" class="form-control">
        <p class="help-block">The name yo daddy gave ya.</p>
      </div>
      <div class="form-group">
        <label>Email</label>
        <input name="user_email" type="text" value="<?php echo $_SESSION['username']; ?>" class="form-control">
        <p class="help-block"><b style="color:red;">NOTE:  If you change this field you will be logged out!</b></p>
      </div>
    </div>
    <div class="col-lg-6">
      <h1>User Password</h1>
      <div class="form-group">
        <label>Password</label>
        <input type="password" name="set_pass" placeholder="*****" class="form-control">
        <p class="help-block">Use this section to change your password</p>
      </div>
      <div class="form-group">
        <label>Confirm Password</label>
        <input type="password" name="set_pass_conf" placeholder="*****" class="form-control">
        <p class="help-block">confirm your password here.</p>
      </div>
            

    </div>
  </div><button type="submit" class="btn btn-primary">Update Profile</button>
</form>
