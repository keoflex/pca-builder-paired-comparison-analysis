<?php
/************************************************************************************************
Home page INDEX
Provides Authenticaty and Session building
Includes dashboard.php if successful
Includes database and functions
Sets timzone for program
provides for logout thorugh ./?logout

Author: Michael Keough
Date Modified: 12/3/2015
************************************************************************************************/


session_start();
date_default_timezone_set('America/Chicago');
//************************************************************************************************
// SITE STRUCTURE AND GUIDLINES
//************************************************************************************************

//***for all $_POST[] functions use mysql_real_escape_string($_POST[]; this provides extra security and prevents hackers from doing a POST hack
//***
//***
//************************************************************************************************
//END GUIDLINES
//************************************************************************************************

 // start up your PHP session! 
//header("Cache-control: private"); // IE 6 Fix. 

//connect to the database config


include "dbcon/config_sqli.php";
include "dbcon/php_functions.php";
$pg_encrypt_key = "4Trin3bm12013formetrics";


$ERROR = "";
$USR_id = 'NA';
if(isset($_SESSION['userid'])){
	$USR_id = pg_encrypt($_SESSION['userid'],$pg_encrypt_key,"decode");
}else if(isset($_GET['email'])){
	$email = $conn->real_escape_string($_GET['email']);
	$email = pg_encrypt($email,$pg_encrypt_key,"decode");
	
	$email = str_replace("&#13;&#10;","",$email);

	//echo $email."test this";
	$loginCheck = "SELECT * FROM users where USR_username='".$email."'";
	
	$res=mysqltng_query($loginCheck);

	if(mysqltng_num_rows($res)!=1){
		//create a user account
		$USR_id = "I CREATED AN ACCOUNT";
		$namebreak = explode("@",$email); //get everything before the @ sign in the email

			$nameArray = explode(".",$namebreak[0]);
			$fname = $nameArray[0];
		
		$create = "insert into users(USR_username,USR_fname,USR_lname) values('".$email."','".$fname."','')";
		//make sure someone is not tryhing to modify the email header
		//serach for non ASCII chars
		if(mb_detect_encoding($email, 'ASCII', true)){
			//check to see if the query has all blank balues ie email is blank
			if($create !== "insert into users(USR_username,USR_fname,USR_lname) values('','','')"){
				if(mysqltng_query($create)){
					
					$loginCheck = "SELECT * FROM users where USR_username='".$email."'";
					//echo $loginCheck;
					$res=mysqltng_query($loginCheck);
					$USR_id = mysqltng_result( $res,0,"USR_id" );
					$_SESSION['userid'] = pg_encrypt($USR_id,$pg_encrypt_key,"encode");
					
				}
			}else{
					$ERROR	= "Email can not be blank";

			}
		}else{
			$ERROR	= "Error: Email addon Error";
		}
	}else{
		//everything checks out so proceed with the next step
		$USR_id = mysqltng_result( $res,0,"USR_id" );

	}

/////////////////////////////////////////////////////////////////////
}		

?>
<!DOCTYPE html>
<html lang="en">

<head>

<?php
include "page_content/header.php";	
if(isset($_GET['email'])){
	$email = $conn->real_escape_string($_GET['email']);
	$email = pg_encrypt($email,$pg_encrypt_key,"decode");
	$email = str_replace("&#13;&#10;","",$email);

}
?>	


</head>

<body >
<center>
<h2 style="color:#ffffff;">Paired Comparitive Analysis </h2>
<h4 style="color:#F98F07"><?php echo $email; ?> <a href="./?logout"> LOG OUT</a></h4>

<?php
if($USR_id !== 'NA'){	

}

?>
</center>
	<div id="page-wrapper">

		<div class="container-fluid">
				<?php
if($ERROR !== ''){
	echo "ERROR PROCESSING PAGE: ".$ERROR;
}else{
//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
// PAGE PROCESSOR: All pages are pulled from $_GET[pg]
//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
if(isset($_GET['code'])){
	if($USR_id !== 'NA'){	
		include "page_content/LIVE/liveForm.php";
	}else{
		include "page_content/LIVE/authenticate.php";
	}
}else{
	include "page_content/LIVE/authenticate.php";

}	
//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
// END PAGE PROCESSOR
//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@	
}

?>
<?php
/*
  		  
*/
?>
		</div>
		<!-- /.container-fluid -->

	</div>
	<!-- /#page-wrapper -->
<div style="width:100%; margin:auto; text-align:center; color:#DBDBDB; padding-top:10px;">
<img style="width:25%" src="../images/logo_grey.png">
<br>
Copyright 2015 keoflex LLC
</div>
</div>
<!-- /#wrapper -->

<!-- jQuery -->
<script src="js/jquery.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="js/bootstrap.min.js"></script>

<!-- Morris Charts JavaScript -->
<script src="js/plugins/morris/raphael.min.js"></script>
<script src="js/plugins/morris/morris.min.js"></script>
<script src="js/plugins/morris/morris-data.js"></script>

<script type="text/javascript">

    function validateForm(formObj) {  
			   
		   
		document.getElementById('val').style.visibility = 'hidden';
		document.getElementById('val2').style.visibility = 'hidden';
		
		document.getElementById('val-span').style.visibility = 'visible';
		document.getElementById('val2-span').style.visibility = 'visible';
	
    }  
	
	function validateFormMultiButton(formObj) {  
		   document.getElementById('val-1').style.visibility = 'hidden';
		   document.getElementById('val-2').style.visibility = 'hidden';
		   document.getElementById('val-3').style.visibility = 'hidden';
		   
		   document.getElementById('val2-1').style.visibility = 'hidden';
		   document.getElementById('val2-2').style.visibility = 'hidden';
		   document.getElementById('val2-3').style.visibility = 'hidden';
		   
		   document.getElementById('val-span').style.visibility = 'visible';
		   document.getElementById('val2-span').style.visibility = 'visible';
		  // document.getElementById('val').disable = true;

    } 
</script>
</body>

</html>
<?php

?>
