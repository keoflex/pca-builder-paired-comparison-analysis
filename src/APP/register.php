<!--

Author: W3layouts

Author URL: http://w3layouts.com

License: Creative Commons Attribution 3.0 Unported

License URL: http://creativecommons.org/licenses/by/3.0/

-->
<?php


	include "dbcon/config_sqli.php";
?>
<!DOCTYPE html>

<html>
<head>
<title>Register for an account</title>
<meta charset="utf-8">
<link href="../css/style.css" rel='stylesheet' type='text/css' />
<meta name="viewport" content="width=device-width, initial-scale=1">
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>



<link href='http://fonts.googleapis.com/css?family=Open+Sans:600italic,400,300,600,700' rel='stylesheet' type='text/css'>


</head>

<body class="register">


<div class="login-form"> <a href="../index.php">
  <div class="head"> <img src="../images/mem2.jpg" alt=""/> </div>
  </a>
  <?php
$Error = 2;

if(isset($_POST["SIGNIN-USERNAME"])){
$username = $_POST["SIGNIN-USERNAME"];	
$password = $_POST["signin-password"];	
$password_conf = $_POST["confirm-password"];	

$fname = '';
$lname = '';
$namebreak = explode("@",$username); //get everything before the @ sign in the email

if(strpos($username,".") !== false){ //if it contains a period assume first.last 
	$nameArray = explode(".",$namebreak[0]);
	$fname = $nameArray[0];
	$lname = $nameArray[1];
}else{
	$fname = $namebreak[0];	
}

$Error = 0;
	if($username == ''){
	?>
  <div style="background:#FF0004; color:#ffffff;"> <b >You must include your email address!<br>
  </div>
  <?php	
	$Error = 1;	
	}
	if($password == ''){
	?>
  <div style="background:#FF0004; color:#ffffff;"> <b >You must include a password<br>
  </div>
  <?php	
	$Error = 1;	
	}
	if($password != $password_conf){
	?>
  <div style="background:#FF0004; color:#ffffff;"> <b >Your passwords must match!<br>
  </div>
  <?php	
	$Error = 1;	
	}
	

		
		
}
	if($Error == 0){
		$check_username = "select * from users where USR_username = '".$username."' and USR_pass = 'nopass'";
		$check_username_qry = mysqltng_query($check_username);
		//echo "num rows = ".mysqltng_num_rows($check_username_qry);
		if(mysqltng_num_rows($check_username_qry) < 1){
			$passwordStrong = sha1($password.$loginSeed);
			
			$qry = "insert into users(USR_username,USR_pass,USR_fname, USR_lname) value('".$username."','".$passwordStrong."','".$fname."','".$lname."')"; //$TA_id is defined at the top of dashboard_main.php
			//echo $qry;
			$QUERY_PROCESS = mysqltng_query($qry);
			if($QUERY_PROCESS){
				?>
			  <div style="position:absolute; top:150px; background:#FFFFFF; width:100%; padding:10px; font-size:36px;">
				<center>
				  <strong>SUCCESS:</strong> Your account has been created.
				</center>
				<form action="../APP" method="post">
				  <input type="hidden" name="SIGNIN-USERNAME" value="<?php echo $username; ?>">
				  <input type="hidden" name="signin-password" value="<?php echo $password; ?>">
				  <input type="submit" onclick="myFunction()" value="SIGN IN" >
				</form>
			  </div>
			  <?php	
			}else{
				?>
			  <div style="position:absolute; top:150px; background:#FFFFFF; width:100%; padding:10px;"> <strong>ERROR!!: </strong> There was a problem setting up your account </div>
			  <?php	
				
			}
		}else{
			?>
			  <div style="position:absolute; top:150px; background:#FFFFFF; width:100%; padding:10px;"> 
              <h1>You already have an account</h1>
              <strong></strong>It looks like you have previously completed a PCA so you already have an account.  We have generated a random password for you and sent it to the email <?php echo $username; ?></div>
			  <?php	
			 
				
				$to = $username;
			
				$subject = 'PCA Password Reset';
				
				$headers = "From: noreply@".$site_name."\r\n";
				$headers .= "Reply-To: ".$site_email."\r\n";
				$headers .= "MIME-Version: 1.0\r\n";
				$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
				$message = '<html><body>';
				$length = 7;
				$randPassword = substr(str_shuffle("0123456789abcdefghjkmnpqrstuvwxyzABCEFGH@$#*!JKLMNPQRSTUVWXYZ"), 0, $length);

				$message .= "<h1>You have attempted to register for a PCA Account</h1>
				We have set your password to ".$randPassword."
				";
				
				$message .= "</body></html>";
				if (mail($to, $subject, $message, $headers)) {
				  echo 'Your message has been sent.';
				} else {
				  echo 'There was a problem sending the email.';
				}
				
				$passwordStrong = sha1($randPassword.$loginSeed);
			
				$qry = "update users set USR_pass = '".$passwordStrong."' where USR_username = '".$username."'"; //$TA_id is defined at the top of dashboard_main.php
				$QUERY_PROCESS = mysqltng_query($qry);
		}

		
		
	}else{
?>
  <form action="" method="post">
    <center>
      <h1 style="font-size:24px;">Register for an account</h1>
      <br>
    </center>
    <li>
      <input tabindex="1" type="email" name="SIGNIN-USERNAME" class="text" required placeholder="Email Address" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = '';}" >
      <a href="#" class=" icon user"></a> </li>
    <li>
      <input tabindex="2" type="password" required name="signin-password" placeholder="signin-password" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Password';}">
      <a href="#" class=" icon lock"></a> </li>
    <li>
      <input tabindex="3" type="password" required name="confirm-password" placeholder="confirm-password" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Password';}">
      <a href="#" class=" icon lock"></a> </li>
    <div class="p-container"> <a href="../index.php">
      <button class="btn" type="button" >BACK</button>
      </a>
      <input tabindex="4" type="submit" onclick="myFunction()" value="REGISTER" >
      <div class="clear"> </div>
    </div>
  </form>
  <?php
	}
?>
</div>

<!--//End-login-form--> 

<!-----start-copyright----> 

<!-----//end-copyright---->

</body>
</html>