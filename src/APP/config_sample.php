<?php

/********************************************************************
See install.php  

You will need to change the (*) fields in order to make the system work
You will also need to make sure you have already setup your database in MySql
I have provided the DB.sql file  so all you need to do is create the database 

/********************************************************************/

$DB_host = 'localhost'; // mysql host
$DB_login = "database_username_here"; // mysql login
$DB_password = "database_password_here"; // mysql password
$DB_database = "database_name_here"; // the database which can be used by the script.

//this see is used to add to all passwords
//This is used to see passwords encrypted with sha1().  Simply helps to further encrypt passwords.

//change this if you want but this is the seed for every password, so if you change it then all user password will be changed
$loginSeed = '~!@265r49ABd*()';  
//password format is sha1(password~!@265r49ABd*()) ie if you password is mysecurepassword then its
//sha1(mysecurepassword~!@265r49ABd*()
//or
//e069e0245c24f3a4e5c57a640112970db8b94527
//NOTE:  in the dashboard.php file I have programed in a password bypass that looks like the following.  YOU WILL WANT TO REMOVE THIS. or make it more secure.

$site_name = 'Sitename.com';  //the domain name you have this program under (not super important, but is used in a few locations
$pg_encrypt_key = "123YoUrPaGeEnCr987"; //each page url is encrypted. This is the key.  Make it anything you want 


$site_email = "youremail@email.com";
//sqli connection functions
$conn = new mysqli($DB_host,$DB_login,$DB_password,$DB_database);
?>