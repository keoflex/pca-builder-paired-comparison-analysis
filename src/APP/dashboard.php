<?php
/*
This is the Main Dashboard of the site where the processing of pages occures
Author: Michael Keough
Date Modified: 12/3/2015
*/

if(isset($DB_host)){//make sure index.php is what is loaded not dashboard
require_once("main_head.php");
?>

    <div id="wrapper">

       <?php

	   
	   include "menu.php";
	   ?>

        <div id="page-wrapper">

            <div class="container-fluid">
            <?php
			if($BASE_URL == "http://localhost/priortyFlex/APP"){
                ?>
                    <div class="col-lg-12">
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <i class="fa fa-info-circle"></i>  <strong>ALERT </strong> Your are on the local server
                        </div>
                    </div>
			<?php
            }
            ?>
                    <?php
			include "dbquery/QUERY_PROCESS_SUB.php";
//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
// PAGE PROCESSOR: All pages are pulled from $_GET[pg]
//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@		

			if((@include $include_address) === false)
				{
					// handle error
					include "page_content/404.php";
				}
		
//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
// END PAGE PROCESSOR
//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@		
   ?>
<?php
/*
              
*/
?>
            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
<div style="width:100%; margin:auto; text-align:center; color:#DBDBDB; padding-top:10px;">
<img style="width:25%" src="../images/logo_grey.png">
<br>
Copyright 2015 keoflex LLC
</div>
    <!-- jQuery 
    <script src="js/jquery.js"></script>
-->
    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>



</body>

</html>
<?php
}else{
	//if dashboard was loaded then refresh to index
		Header("Location: ./");    // redirect him to protected.php
	
}
?>
