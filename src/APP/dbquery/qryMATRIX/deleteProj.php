<?php
/*
This is a new Matrix Query
Author: Michael Keough
Date Modified: 12/3/2015
*/


$element = "PCA Project";
$element_function = "DELETED";
//Define Variables for the form

$PROJ_id = $conn->real_escape_string($_POST["delid"]);

$PROJ_date = date('Y/m/d');
	//form query
	$qry_itmes = "DELETE FROM projects_items where PROJ_id = ".$PROJ_id;
	mysqltng_query($qry_itmes);
	
	$qry_res = "DELETE FROM projects_results where PROJ_id = ".$PROJ_id;
	mysqltng_query($qry_res);
	
	$qry = "DELETE FROM projects where PROJ_id =".$PROJ_id;
	///echo $qry;
	
	
	$QUERY_PROCESS = mysqltng_query($qry);
	//call query process to make sure there are not errors in the query
	require_once("dbquery/QUERY_PROCESS.php");

?>