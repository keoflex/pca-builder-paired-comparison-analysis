<?php
/*
This is a new Matrix Query
Author: Michael Keough
Date Modified: 12/3/2015
*/

$PROJ_name = $conn->real_escape_string($_POST["matrix_name"]);

$element = "PCA named <b>$PROJ_name</b>";
$element_function = "Updated";
//Define Variables for the form
$PROJ_button = $conn->real_escape_string($_POST["button_header"]);

$PROJ_public = $conn->real_escape_string($_POST["make_public"]);
$PROJ_weight = $conn->real_escape_string($_POST["matrix_weight"]);
//$PROJ_time = $conn->real_escape_string($_POST["matrix_time"]);
$PROJ_date = date('Y/m/d');
$PROJ_id = pg_encrypt($_POST["project_code"],$pg_encrypt_key,"decode");
$PROJ_id = str_replace("@","",$PROJ_id);
	//form query
	$qry = "UPDATE projects set PROJ_public = ".$PROJ_public.",PROJ_weight=".$PROJ_weight.",PROJ_button='".$PROJ_button."' , PROJ_name = '".$PROJ_name."' where PROJ_id =".$PROJ_id;
	//echo $qry;
	
	
	$QUERY_PROCESS = mysqltng_query($qry);
	//call query process to make sure there are not errors in the query
	require_once("dbquery/QUERY_PROCESS.php");

?>