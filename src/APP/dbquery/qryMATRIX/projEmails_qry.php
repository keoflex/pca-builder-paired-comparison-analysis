<?php
/*
This is a new Matrix Query
Author: Michael Keough
Date Modified: 12/3/2015
*/

$save = $_POST['send'];
$element_val = "Saved and Sent";
if($save=="Save"){
	$element_val = "Saved";	
}
$element = "Email List ";
$element_function = $element_val;
$PROJ_emails = $conn->real_escape_string($_POST['email_list']);
$PROJ_emails = str_replace('\r\n',',',$PROJ_emails);

$PROJ_email_encode = pg_encrypt($PROJ_emails,$pg_encrypt_key,"encode");

//Define Variables for the form


$PROJ_id = pg_encrypt($_POST["project_code"],$pg_encrypt_key,"decode");
$PROJ_id = str_replace("@","",$PROJ_id);
	//form query
	$qry = "UPDATE projects set PROJ_emails = '".$PROJ_email_encode."' where PROJ_id =".$PROJ_id;
	echo $qry;
	if($save!=="Save"){
		
		$PROJ_email_exp = explode(',',$PROJ_emails);
		foreach($PROJ_email_exp as $email){
			if($email !== ''){
				$to = $email;
			
				$subject = 'Please complete this Paired Comparitive Analysis';
				
				$headers = "From: " . $_SESSION['username'] . "\r\n";
				$headers .= "Reply-To: ". strip_tags($_SESSION['username']) . "\r\n";
				$headers .= "MIME-Version: 1.0\r\n";
				$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
				$message = '<html><body>';
				
				$message .= "<h1>Please click the link below to access the form.</h1>
				".$BASE_URL."/matrix.php?code=".$_POST["project_code"]."&email=".pg_encrypt($email,$pg_encrypt_key,"encode");
				
				$message .= "</body></html>";
				if (mail($to, $subject, $message, $headers)) {
				  echo 'Your message has been sent.';
				} else {
				  echo 'There was a problem sending the email.';
				}
			}
		}
	}

	
	
	$QUERY_PROCESS = mysqltng_query($qry);
	//call query process to make sure there are not errors in the query
	require_once("dbquery/QUERY_PROCESS.php");

?>