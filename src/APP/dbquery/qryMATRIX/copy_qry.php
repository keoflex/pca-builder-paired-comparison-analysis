<?php
/*
This is a new Matrix Query
Author: Michael Keough
Date Modified: 12/3/2015
*/

$element = "PCA</b>";
$element_function = "Copied";
//Define Variables for the form
$PROJ_name = $conn->real_escape_string($_POST["PROJ_name"]);
$PROJ_button = $conn->real_escape_string($_POST["PROJ_button"]);
$PROJ_weight = $conn->real_escape_string($_POST["PROJ_weight"]);


$copy_info =  $conn->real_escape_string(pg_encrypt($_POST["copy_info"],$pg_encrypt_key,"decode"));
$copy_info = stripcslashes($copy_info);
$copy_info = str_replace('PROJ_id_val','(select PROJ_id from projects where PROJ_name = "'.$PROJ_name.'" and USR_id = '.
$USR_id.' order by PROJ_id desc limit 1)',$copy_info);
$qry = str_replace('values,','values',$copy_info);

$create_copy_query = "INSERT INTO projects(PROJ_name,PROJ_date,USR_id,PROJ_button) values('".$PROJ_name."','".date('Y-m-d')."',".$USR_id.",'".$PROJ_button."')";
mysqltng_query($create_copy_query);


	$QUERY_PROCESS = mysqltng_query($qry);
	//call query process to make sure there are not errors in the query
	require_once("dbquery/QUERY_PROCESS.php");

?>