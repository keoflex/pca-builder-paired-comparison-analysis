<?php
/*
Delete Entries for a user on a project
Author: Michael Keough
Date Modified: 12/3/2015
*/


$element = "PCA Entries";
$element_function = "DELETED";
//Define Variables for the form

$USR_id_results = $conn->real_escape_string($_POST["delid"]);
$PROJ_id = $conn->real_escape_string($_POST["proj"]);

	//form query
	$qry = "DELETE FROM projects_results where PROJ_id = ".$PROJ_id." and USR_id = ".$USR_id_results;
	
	echo $qry;
	
	
	$QUERY_PROCESS = mysqltng_query($qry);
	//call query process to make sure there are not errors in the query
	require_once("dbquery/QUERY_PROCESS.php");

?>