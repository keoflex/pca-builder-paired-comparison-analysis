Ok so im lazy and I didn't build an install.php :-/  Maybe in a future release. 

Here is what you need to do to install this program

1. Either upload your code to a web hosting provider like lunarpages.com or download WAMP and install it local on a computer
### Yes there are other options, but for the new and non-technical, this will get you started

2. If using WAMP go to http://localhost/phpmyadmin in a browser and on a new install log in as root with no password
   If using a webserver you will need to log into you CPanel or other management console and find MySql
   		Create the database
		Then access it via phpmyadmin (accessable through your console)
		
3.  Once you are in PHPMyAdmin and the database is created, click that database and then click import or upload.  you will be uploading the DB.sql file
	Alternately you can rename DB.sql to DB.txt and copy all the code.  Then click the database and choose  the SQL option at the top
	Past the code in the big white box and click submit.
	
4. Go to the config_sample.php file and put in your database credentials
5. Rename config_sample.php to config.php
6. if you go to the site (http://localhost/APP/register.php) (using wamp) or (yoursite.com/APP/register.php) and it loads then you did everything right!!

