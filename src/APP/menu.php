<?php
/*
Side Menu system 
Author: Michael Keough
Date Modified: 12/3/2015
*/
?>
<!-- Navigation -->

<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation"> 
  <!-- Brand and toggle get grouped for better mobile display -->
  <div class="navbar-header">
    <button  tabindex="-1" type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
    <a  tabindex="-1" class="navbar-brand" href="./"><!--DISD Priority Matrix--><img style="position:absolute; top:0px;" width="100px" src="../images/logo.png"><span style="margin-left:100px;">  <b style="color:#FCFF99; margin-left:20px;">P</b>aired <b style="color:#FCFF99;"> C</b>omparitive <b style="color:#FCFF99;">A</b>nalysis (<b style="color:#FCFF99;">PCA</b>) Tool</span></a> </div>
  <!-- Top Menu Items -->
  <ul class="nav navbar-right top-nav">
    
    <li class="dropdown"> <a  tabindex="-1" href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bell"></i> <b class="caret"></b></a>
      <ul class="dropdown-menu alert-dropdown">
        
        <?php

$file = file("page_content/SETTINGS/change_log.txt");
$file = array_reverse($file);
$count = 0;
foreach($file as $f){
	if($count == 6){
		break;
	}else{
	$f_array = explode(',',$f);
	?>
            <li> <a  tabindex="-1" href="./index.php?pg=<?php echo pg_encrypt("SETTINGS-change_log",$pg_encrypt_key,"encode") ?>"><?php echo $f_array[0]; ?> <span class="label label-<?php echo $f_array[1]; ?>"><?php echo $f_array[2]; ?></span></a> </li>

    <?php
	$count++;
	}
}
?> 
       <!--
        <li> <a  tabindex="-1" href="#">Alert Name <span class="label label-primary">Alert Badge</span></a> </li>
        <li> <a  tabindex="-1" href="#">Alert Name <span class="label label-success">Alert Badge</span></a> </li>
        <li> <a  tabindex="-1" href="#">Alert Name <span class="label label-info">Alert Badge</span></a> </li>
        <li> <a  tabindex="-1" href="#">Alert Name <span class="label label-warning">Alert Badge</span></a> </li>
        <li> <a  tabindex="-1" href="#">Alert Name <span class="label label-danger">Alert Badge</span></a> </li>
        <li class="divider"></li>
        <li> <a  tabindex="-1" href="#">View All</a> </li>
        -->
              </ul>
    </li>
    <li class="dropdown"> <a  tabindex="-1" href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php echo $fname." ".$lname; ?> <b class="caret"></b></a>
      <ul class="dropdown-menu">
        <li> <a  tabindex="-1" href="./index.php?pg=<?php echo pg_encrypt("PROFILE-index",$pg_encrypt_key,"encode") ?>"><i class="fa fa-fw fa-user"></i> Profile</a> </li>
        <li class="divider"></li>
        <li> <a  tabindex="-1" href="index.php?logout"><i class="fa fa-fw fa-power-off"></i> Log Out</a> </li>
      </ul>
    </li>
  </ul>
  <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
  <div class="collapse navbar-collapse navbar-ex1-collapse">
    <ul class="nav navbar-nav side-nav">
<?php
if(!isset($folder)){
	$folder = "";
}
?> 
      <li <?php if($folder == "") echo "class='active'"; ?>> <a  tabindex="-1" href="dashboard.php"><i class="fa fa-fw fa-dashboard"></i> Dashboard </a> </li>
 <!-- My Matrices --> 
    
      <li <?php if($folder == "MY") echo "class='active'"; ?>> <a  tabindex="-1" href="javascript:;" data-toggle="collapse" data-target="#Matrices"><i class="fa fa-fw fa-folder-open-o"></i>INVOLVMENT<i class="fa fa-fw fa-caret-down"></i></a>
        <ul id="Matrices" class="<?php if($folder == "MY") echo "expand"; else echo "collapse"; ?>">
         <!-- <li> <a  tabindex="-1" style="color:red;" href="./index.php?pg=<?php echo pg_encrypt("MY-active",$pg_encrypt_key,"encode") ?>">Active / In Progress</a> </li> -->
          <li> <a  tabindex="-1"  href="./index.php?pg=<?php echo pg_encrypt("MY-history",$pg_encrypt_key,"encode") ?>">My History</a> </li>
        </ul>
      </li>
 <!-- Design Center -->    
      <li  <?php if($folder == "DESIGN") echo "class='active'"; ?>> <a  tabindex="-1" href="javascript:;" data-toggle="collapse" data-target="#Design"><i class="fa fa-fw fa-edit"></i>Design Center<i class="fa fa-fw fa-caret-down"></i></a>
        <ul id="Design" class="<?php if($folder == "DESIGN") echo "expand"; else echo "collapse"; ?>">
          <li> <a  tabindex="-1" href="./index.php?pg=<?php echo pg_encrypt("DESIGN-new",$pg_encrypt_key,"encode") ?>">New PCA</a> </li>
          <!--
          <li> <a  tabindex="-1" href="./index.php?pg=<?php echo pg_encrypt("DESIGN-continue",$pg_encrypt_key,"encode") ?>">Continue Editing</a> </li>-->
          <li> <a  tabindex="-1" href="./index.php?pg=<?php echo pg_encrypt("DESIGN-list",$pg_encrypt_key,"encode") ?>">PCA List</a> </li>
         <?php
/*
          <li> <a  tabindex="-1" style="color:red;" href="./index.php?pg=<?php echo pg_encrypt("MY-________________",$pg_encrypt_key,"encode") ?>">Closed Matrices</a> </li> */ ?>
        </ul>
      </li>
 <?php
/*
 <!-- Community -->    
      <li <?php if($folder == "COMMUNITY") echo "class='active'"; ?>> <a  tabindex="-1" href="javascript:;" data-toggle="collapse" data-target="#Community"><i class="fa fa-fw fa-users"></i>Community<i class="fa fa-fw fa-caret-down"></i></a>
        <ul id="Community" class="<?php if($folder == "COMMUNITY") echo "expand"; else echo "collapse"; ?>">
          <li> <a  tabindex="-1" style="color:red;" href="./index.php?pg=<?php echo pg_encrypt("MY-active",$pg_encrypt_key,"encode") ?>">Search Public</a> </li>
          <li> <a  tabindex="-1" style="color:red;" href="./index.php?pg=<?php echo pg_encrypt("MY-________________",$pg_encrypt_key,"encode") ?>">Direct Add</a> </li>
        </ul>
      </li>
<?php
*/
/*
 <!-- Reports -->    
      <li <?php if($folder == "REPORTS") echo "class='active'"; ?>> <a  tabindex="-1" href="javascript:;" data-toggle="collapse" data-target="#Reports"><i class="fa fa-fw fa-bar-chart-o"></i>Reports<i class="fa fa-fw fa-caret-down"></i></a>
        <ul id="Reports" class="<?php if($folder == "REPORTS") echo "expand"; else echo "collapse"; ?>">
          
          <li> <a  tabindex="-1" style="color:red;" href="./index.php?pg=<?php echo pg_encrypt("MY-________________",$pg_encrypt_key,"encode") ?>">totals</a> </li>
          
        </ul>
      </li> 
*/
?>     
    </ul>
  </div>
  <!-- /.navbar-collapse --> 
</nav>
